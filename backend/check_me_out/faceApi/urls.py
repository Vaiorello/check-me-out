# from rest_framework.routers import DefaultRouter
from .views import *
from django.urls import path, include


# router = DefaultRouter()

# router.register('getFaceByImage', get_faces_by_image,
#                 basename='faces_by_image')
# router.register('getFaceByURL', get_faces_by_url, basename='faces_by_url')

# urlpatterns = router.urls

urlpatterns = [
    path('getFaceByImage/', get_faces_by_image, name="api-face-image"),
    path('getFaceByURL/', get_faces_by_url, name="api-face-url")
]
