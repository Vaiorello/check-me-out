import json
import base64
from rest_framework.decorators import api_view
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from PIL import Image
import cv2
import io
import requests
import numpy as np


def stringToRGB(base64_string):
    imgdata = base64.b64decode(str(base64_string))
    return io.BytesIO(imgdata)


@api_view(['POST'])
@csrf_exempt
def get_faces_by_image(request):

    image = json.loads(request.body.decode('utf-8'))
    image = image['image'].split(',')
    image = image[1]

    endpoint = 'https://viorel.cognitiveservices.azure.com/bing/v7.0/images/visualsearch'
    subscription_key = 'cdcb312dd1f84bcab7cd99004bf41915'
    HEADERS = {'Ocp-Apim-Subscription-Key': subscription_key}
    file = {'image': ('myfile', stringToRGB(image))}

    try:
        response = requests.post(endpoint, headers=HEADERS, files=file)
        response.raise_for_status()
        result_images = response.json()

    except Exception as ex:
        raise ex

    data = []

    for r in result_images['tags'][0]['actions'][2]['data']['value'][:24]:
        data.append({
            'name': r['name'],
            'url': r['contentUrl']
        })

    return JsonResponse({'data': json.dumps(data)})


@api_view(['POST'])
@csrf_exempt
def get_faces_by_url(request):

    url = json.loads(request.body.decode("UTF-8"))
    url = url['image']

    endpoint = 'https://viorel.cognitiveservices.azure.com/bing/v7.0/images/visualsearch'
    subscription_key = 'cdcb312dd1f84bcab7cd99004bf41915'
    HEADERS = {'Ocp-Apim-Subscription-Key': subscription_key,
               "Content-Disposition": "form-data; name=knowledgeRequest"}
    body = {
        "imageInfo": {
            "url": url
        }
    }

    try:
        response = requests.post(
            endpoint, headers=HEADERS, json=body)
        response.raise_for_status()
        result_images = response.json()

    except Exception as ex:
        raise ex

    data = []

    for r in result_images['tags'][0]['actions'][2]['data']['value'][:24]:
        data.append({
            'name': r['name'],
            'url': r['contentUrl']
        })

    return JsonResponse({'data': json.dumps(data)})
