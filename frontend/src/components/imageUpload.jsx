import React, { Component } from "react";
import { Upload, message, Row, Col } from "antd";
import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import ImgCrop from "antd-img-crop";

function beforeUpload(file) {
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
        message.error("Poti incarca doar fisiere JPG/PNG!");
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        message.error("Imaginea trebuie sa fie mai mica de 2MB!");
    }
    return isJpgOrPng && isLt2M;
}

class ImageUpload extends Component {
    state = {
        loading: false,
    };

    render() {
        const uploadButton = (
            <div>
                {this.state.loading ? <LoadingOutlined /> : <PlusOutlined />}
                <div className="ant-upload-text">Upload</div>
            </div>
        );
        const { imageUrl, handleChange } = this.props;
        return (
            <div>
                <Row justify="center">
                    <Col>
                        <div style={{ textAlign: "center" }}>
                            <h1>
                                Incarca o imagine si iti voi arata cu cine
                                semeni
                            </h1>
                            <Row justify="center">
                                <Col>
                                    <ImgCrop rotate>
                                        <Upload
                                            name="avatar"
                                            listType="picture-card"
                                            className="avatar-uploader"
                                            showUploadList={false}
                                            action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                                            beforeUpload={beforeUpload}
                                            onChange={handleChange}
                                            style={{
                                                textAlign: "center",
                                            }}
                                        >
                                            {imageUrl ? (
                                                <img
                                                    src={imageUrl}
                                                    alt="avatar"
                                                    style={{ width: "100%" }}
                                                />
                                            ) : (
                                                uploadButton
                                            )}
                                        </Upload>
                                    </ImgCrop>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default ImageUpload;
