import React from "react";
import { Card } from "antd";

const { Meta } = Card;

const ImageCard = (props) => {
    const { image, name } = props;
    return (
        <div>
            <Card
                hoverable
                style={{ width: 240 }}
                cover={<img src={image} alt="result" />}
            >
                <Meta title={name} />
            </Card>
        </div>
    );
};

export default ImageCard;
