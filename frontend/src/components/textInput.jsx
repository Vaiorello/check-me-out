import React from "react";
import { Input, Tooltip, Row, Col } from "antd";
import { InfoCircleOutlined, LinkOutlined } from "@ant-design/icons";

const TextInput = (props) => {
    const { url, onChange } = props;
    return (
        <div>
            <Row justify="center">
                <Col>
                    <div style={{ textAlign: "center" }}>
                        <h1>Incarca un link si iti voi arata cu cine semeni</h1>
                        <Input
                            name="url"
                            defaultValue={url}
                            onChange={onChange}
                            type="text"
                            placeholder="Introdu linkul"
                            prefix={
                                <LinkOutlined className="site-form-item-icon" />
                            }
                            suffix={
                                <Tooltip title="Introduci un link care duce catre o imagine">
                                    <InfoCircleOutlined
                                        style={{ color: "rgba(0,0,0,.45)" }}
                                    />
                                </Tooltip>
                            }
                        />
                    </div>
                </Col>
            </Row>
            <br />
        </div>
    );
};

export default TextInput;
