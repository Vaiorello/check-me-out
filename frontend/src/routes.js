import React from "react";
import { Route } from "react-router-dom";

import Search from "./containers/search";

const BaseRouter = () => {
    return (
        <div>
            <Route exact path="/" component={Search} />
        </div>
    );
};

export default BaseRouter;
