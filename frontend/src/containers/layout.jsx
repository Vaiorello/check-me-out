import React, { Component } from "react";
import { Layout } from "antd";
import "../css/styles.css";

const { Header, Content } = Layout;

class CustomLayout extends Component {
    constructor(props) {
        super(props);
        this.wrapper = React.createRef();
    }

    render() {
        return (
            <Layout className="layout">
                <Header>
                    {/* <div className="logo" /> */}
                    <h1 style={{ color: "white" }}>Check Me Out</h1>
                </Header>
                <Content>
                    <div className="site-layout-content">
                        <div ref={this.wrapper}>{this.props.children}</div>
                    </div>
                </Content>
            </Layout>
        );
    }
}

export default CustomLayout;
