import React, { Component } from "react";
import { Button, Row, Col, message } from "antd";
import { SearchOutlined } from "@ant-design/icons";

import ImageUpload from "../components/imageUpload";
import TextInput from "../components/textInput";
import ImageCard from "../components/imageCard";

import axios from "axios";

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
}

class Search extends Component {
    state = {
        isImage: true,
        buttonText: "URL",
        url: "",
        images: [],
        buttonLoading: false,
    };

    handleChange = (info) => {
        if (info.file.status === "uploading") {
            this.setState({ loading: true });
            return;
        }
        if (info.file.status === "done") {
            getBase64(info.file.originFileObj, (imageUrl) =>
                this.setState({
                    imageUrl,
                    loading: false,
                })
            );
        }
    };

    handleSubmit = () => {
        axios.defaults.headers = {
            Accept: "application/json",
            "Content-Type": "application/json",
        };
        var self = this;
        if (this.state.isImage === true) {
            if (this.state.imageUrl === undefined)
                message.error("Trebuie sa incarcati o poza!");
            else {
                self.setState({ buttonLoading: true });
                axios({
                    method: "post",
                    url: "http://127.0.0.1:8000/api/getFaceByImage/",
                    data: {
                        image: this.state.imageUrl,
                    },
                })
                    .then(function (response) {
                        self.setState({
                            images: JSON.parse(response.data.data),
                        });
                        self.setState({ buttonLoading: false });
                    })
                    .catch(function (error) {
                        console.error(error);
                    });
            }
        } else {
            if (this.state.url === "")
                message.error("Trebuie sa introduceti un link!");
            else {
                axios({
                    method: "post",
                    url: "http://127.0.0.1:8000/api/getFaceByURL/",
                    data: {
                        image: this.state.url,
                    },
                })
                    .then(function (response) {
                        console.log(response);
                        // self.setState({
                        //     images: JSON.parse(response.data.data),
                        // });
                        // console.log(self.state);
                    })
                    .catch(function (error) {
                        console.error(error);
                    });
            }
        }
    };

    changeMethod = () => {
        this.setState({ isImage: !this.state.isImage });
        if (this.state.isImage === false) this.setState({ buttonText: "URL" });
        else this.setState({ buttonText: "Imagine" });
    };

    handleChangeURL = (e) => {
        this.setState({ url: e.target.value });
    };

    render() {
        return (
            <div>
                <Row justify="center">
                    <Col>
                        Incearca si cu &nbsp;
                        <Button onClick={this.changeMethod} type="default">
                            {this.state.buttonText}
                        </Button>
                    </Col>
                </Row>
                {this.state.isImage ? (
                    <ImageUpload
                        handleChange={this.handleChange}
                        imageUrl={this.state.imageUrl}
                    />
                ) : (
                    <TextInput
                        url={this.state.url}
                        onChange={this.handleChangeURL}
                    />
                )}
                <Row justify="center">
                    <Col>
                        {this.state.buttonLoading ? (
                            <Button type="primary" loading>
                                Loading
                            </Button>
                        ) : (
                            <Button
                                type="primary"
                                onClick={this.handleSubmit}
                                icon={<SearchOutlined />}
                            >
                                Cauta
                            </Button>
                        )}
                    </Col>
                </Row>
                <br />
                <Row justify="center" align="middle">
                    {this.state.images.map((imgData, i) => {
                        return (
                            <Col key={i} xl={4} lg={6} md={12} sm={24}>
                                <div
                                    style={{
                                        display: "inline-flex",
                                        justifyContent: "center",
                                        alignItems: "center",
                                    }}
                                >
                                    <ImageCard
                                        image={imgData.url}
                                        name={imgData.name}
                                        style={{
                                            display: "inline-block",
                                            verticalAlign: "middle",
                                        }}
                                    />
                                </div>
                            </Col>
                        );
                    })}
                </Row>
            </div>
        );
    }
}

export default Search;
