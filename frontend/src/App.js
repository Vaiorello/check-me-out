import React, { Component } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import "antd/dist/antd.css";
import BaseRouter from "./routes";

import CustomLayout from "./containers/layout";

class App extends Component {
    render() {
        return (
            <div>
                <Router>
                    <CustomLayout>
                        <BaseRouter></BaseRouter>
                    </CustomLayout>
                </Router>
            </div>
        );
    }
}

export default App;
